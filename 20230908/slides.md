---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### September 8, 2023
---
# IRIF Back-to-Work Meeting

 - September 11, 14:00–18:00
 - Amphi 9E, Halle aux Farines 
 - [Program](https://www.irif.fr/rencontres/irif/rentree2023)
---
# Newcomers
 - ![height:100px](https://sarahwinter.net/profile.jpg) [Sarah Winter](https://sarahwinter.net/), assoc. prof.
 - ![height:100px](http://libk.in/skimg1.jpg) [Leonid Libkin](http://libk.in/), industr. chair IRIF/CNRS
 - ![height:100px](http://igm.univ-mlv.fr/~juge/misc/photo.jpg) [Vincent Jugé](http://igm.univ-mlv.fr/~juge/), deleg. CNRS
 - ![height:100px](https://www.univ-orleans.fr/lifo/Members/Nicolas.Ollinger/no.jpg) [Nicolas Ollinger](https://www.univ-orleans.fr/lifo/Members/Nicolas.Ollinger/), deleg. CNRS
---
## Automata News

 - looking for **seminar speakers**: contact [Alexandra](https://www.irif.fr/~rogova/) and [Marie](https://www.csc.liv.ac.uk/~mfortin/)
 
 - team repository of **internship/PhD proposals**?
   - **Hichma Kari**, 4th y. @ ENS Lyon, looking for 4–6 mo. internship

 - current visitors: [Alexander Rabinovich](http://www.cs.tau.ac.il/~rabinoa/), [Alfredo Costa](https://apps.uc.pt/mypage/faculty/uc26448/en)

 - [Aliaume Lopez' PhD Defense](http://www.lsv.fr/~lopez/posts/2023-08-23.html), Sep. 12
---
## Some IRIF News

 - 🚧 works in the building should end on **Sep. 15**
 - 📝 new [form for event organisation](https://evenement.irif.fr/)
 - 🚅 new [CNRS service for travel](https://esr-cnrs.notilus-inone.fr/)
 - 🧑‍🔬 looking for [volunteers](https://framaforms.org/fete-de-la-science-2023-du-6-au-16-octobre-2023-1686835173) for _Fête de la Science_, **Oct. 6–16**
 - ![height:32px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) [HCERES visit](https://www.irif.fr/intranet/hceres/2017-22/index), Nov. 28–30
---
## 💰 Funding

 - 🎓 [formation](https://www.irif.fr/_media/intranet/lettre-irif/2023-09-08/formulaire_d_inscription_a_une_formation_spv.docx) _Soumettre son projet détaillé à l'ANR_, Sep. 25
 - ![height:26px](https://anr.fr/typo3conf/ext/anr_skin/Resources/Public/assets/img/anr-logo-2021-complet.png) [ANR generic proposals](https://anr.fr/fr/detail/call/aapg-appel-a-projets-generique-2024/), deadline Oct. 19
 - ![height:36px](https://www.horizon-europe.gouv.fr/sites/default/files/2021-05/Logo_Horizon_Europe_1_2.png) [ERC starting grants](https://www.horizon-europe.gouv.fr/calendriers-et-documents-des-appels-erc-27857), deadline Oct. 24
 - ![height:36px](https://www.horizon-europe.gouv.fr/sites/default/files/2021-05/Logo_Horizon_Europe_1_2.png) [MSCA and Citizens](https://www.horizon-europe.gouv.fr/actions-marie-sklodowska-curie-et-les-citoyens-msca-and-citizens-27893), deadline Oct. 25
---
## 📅 Upcoming Deadlines

 - [ICDT 2024](https://dastlab.github.io/edbticdt2024/), Paestum, Italy, deadline Sep. 13
 - [STACS 2024](https://stacs2024.limos.fr/), Clermont-Ferrand, France, deadline Sep. 28
 - [ETAPS 2024](https://etaps.org/2024/cfp/), Luxembourg, deadline Oct. 12
 - [PODS 2024](https://2024.sigmod.org/calls_papers_pods_research.shtml), Santiago, Chile, deadline Dec. 4
---
## 📅 Upcoming Events

 - [Concur 2023](https://www.uantwerpen.be/en/conferences/confest-2023/concur/), Antwerp, Belgium, Sep. 19–22
 - reminder: IRIF [supports](https://www.irif.fr/intranet/irif-environnement/texte-conferences-v2) its members wishing to attend conferences remotely
---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### December 15, 2023

---
## ☃️ Holiday Closure

 - from Dec. 23 to Jan. 2
 - send email before **Dec. 18** to [secretariat+vacation@irif.fr](mailto:secretariat+vacation@irif.fr) if you need access

---
## 📢 Open Positions in 2024

 - 3 tenured positions in the CS department/IRIF
 - open to all the fields in theoretical computer science
   - 1 full professor position
   - 2 associate professor positions
 - **advertise** to your colleagues!
 
---
## ![height:48px](https://www.cnrs.fr/sites/default/files/logo/logo.svg) CNRS Applications

 - deadline for applications **Feb. 9**
 - talks by applicants in the team:
   - Dec. 8, 2pm: [Anantha Padmanabha](https://padmanabha-anantha.github.io/website/#/)
   - Dec. 15, 2pm: [Chana Weil-Kennedy](https://chana-wk.github.io/)
   - Dec. 18, 11am: [Munyque Mittelmann](https://sites.google.com/view/mittelmann)
   - Jan. 15, 11am: [Raphaël Berthon](https://moves.rwth-aachen.de/people/raphael-berthon/)
 - also [Emily Clement](https://perso.eleves.ens-rennes.fr/people/emily.clement/) and [Tito Nguyễn](https://nguyentito.eu) 

---

## [Blurb](https://zulip.irif.fr/#narrow/stream/82-Team-automata-.28public.29/topic/Internship.20blurb/near/4144) for the Team's Webpage

<style scoped>
p,ul {
  font-size: 20pt;
  text-align: left;
  color: #333;
}
h2 + p {
  color: #b81e0d;
}
</style>
Internships

The *automata and applications* team welcomes interns every year to conduct research in theoretical computer science, including (but not exclusively) topics in
 - automata and algebra, with connections to circuit complexity
 - categorical semantics for automata
 - graphs and logic
 - graph databases
 - incomplete databases
 - continuous fractions
 - word combinatorics

Prospective applicants are encouraged to browse the members' pages and contact them directly.

---
## Automata Seminar

 - [Marie Fortin](https://www.irif.fr/~mfortin/) and [Alexandra Rogova](https://www.irif.fr/~rogova/) would welcome your suggestions for speakers starting in February next year

---
## 📅 Upcoming Deadlines

 - [ESSLLI 2024](https://2024.esslli.eu/), Leuven, Belgium, deadline for course titles Dec. 15 and course proposals Jan. 12
 - [LICS 2024](https://lics.siglog.org/lics24/), Talinn, Estonia, deadline Jan. 21
 - [ICALP 2024](https://compose.ioc.ee/icalp2024/), Talinn, Estonia, deadline Feb. 14
 
---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
# Slides for the team meetings

## TODO

```
git remote rename origin old-origin
git remote add origin git@gitlab.math.univ-paris-diderot.fr:schmitz/automata-team.git
```

## 2024

### January 2024

 - [Jan. 26, 2024](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20240126/index.html?job=marp-build)
 - [Jan. 19, 2024](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20240119/index.html?job=marp-build)
 - [Jan. 12, 2024](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20240112/index.html?job=marp-build)
 
## 2023

### December 2023

 - [Dec. 15, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20231215/index.html?job=marp-build)
 - [Dec. 08, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20231208/index.html?job=marp-build)

### November 2023

 - [Nov. 24, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20231124/index.html?job=marp-build)
 - [Nov. 10, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20231110/index.html?job=marp-build)

### October 2023
 - [Oct. 13, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20231013/index.html?job=marp-build)
 - [Oct. 6, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20231006/index.html?job=marp-build)

### September 2023
 - [Sep. 29, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230929/index.html?job=marp-build)
 - [Sep. 22, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230922/index.html?job=marp-build)
 - [Sep. 08, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230908/index.html?job=marp-build)

### June 2023

 - [Jun. 30, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230630/index.html?job=marp-build)
 - [Jun. 23, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230623/index.html?job=marp-build)
 - [Jun. 16, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230616/index.html?job=marp-build)
 - [Jun. 2, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230602/index.html?job=marp-build)

### May 2023

 - [May 26, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230526/index.html?job=marp-build)

### April 2023

 - [Apr. 21, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230421/index.html?job=marp-build)

### March 2023

 - [Mar. 31, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230331/index.html?job=marp-build)
 - [Mar. 24, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230324/index.html?job=marp-build)
 - [Mar. 17, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230317/index.html?job=marp-build)
 - [Mar. 10, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230310/index.html?job=marp-build)

### February 2023

 - [Feb. 24, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230224/index.html?job=marp-build)
 - [Feb. 17, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230217/index.html?job=marp-build)
 - [Feb. 10, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230210/index.html?job=marp-build)
 - [Feb. 3, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230203/index.html?job=marp-build)

### January 2023

 - [Jan. 6, 2023](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20230106/index.html?job=marp-build)

## 2022

### December 2022

 - [Dec. 9, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20221209/index.html?job=marp-build)
 - [Dec. 2, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20221202/index.html?job=marp-build)

### November 2022

 - [Nov. 25, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20221125/index.html?job=marp-build)
 - [Nov. 18, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20221118/index.html?job=marp-build)

### October 2022

 - [Oct. 14, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20221014/index.html?job=marp-build)
 - [Oct. 07, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20221007/index.html?job=marp-build) 
 - [Sept. 30, 2022](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/-/jobs/artifacts/master/raw/public/20220930/index.html?job=marp-build)
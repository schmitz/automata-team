---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### June 16, 2023
---
## 📅 ASV general assembly

 - Tuesday, June 20, 2pm–4pm
   * Human ressources and financial report
     * Engineers for software development
     * PR & MCF positions
   * Requests for resources
     * Any requests?
   * Events
     * Proposals for *IRIF Distinguished Speakers*?
---
## 📅 Upcoming Deadlines

 - [FSTTCS 2023](https://www.fsttcs.org.in/2023/), Hyderabad, India, deadline July 12
 - [CSL 2024](https://csl2024.github.io/Home/), Naples, Italy, deadline July 24
---
## 📅 Summer Schools

 - [EJCIM 2023](https://ejcim23.sciencesconf.org/), Poitiers, Jun. 19–23 
 - [ESSLLI 2023](https://2023.esslli.eu/), Ljubljana, Jul. 31–Aug. 11

---
## 📅 Upcoming Conferences

 - [PODS 2023](https://2023.sigmod.org/), Seattle, USA, Jun. 18–23
 - [LICS 2023](https://lics.siglog.org/lics23/), Boston, USA, Jun. 26–29
 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, Germany, Jul. 10–14
 - [HCRW 2023](https://highlights-conference.org/2023/hcrw), Kassel, Germany, Jul. 17–23
 - [CAV 2023](http://www.i-cav.org/2023/), Paris, France, Jul. 17–22
 - [Highlights 2023](https://highlights-conference.org/2023/), Kassel, Germany, Jul. 24–28
 - [MFCS 2023](https://mfcs2023.labri.fr/), Bordeaux, France, Aug. 28–Sep. 1
 - [VLDB 2023](https://vldb.org/2023/), Vancouver, Canada, Aug. 28–Sep. 1
 - [KR 2023](https://kr.org/KR2023/), Rhodes, Greece, Sep. 2–8
 
---
# Teaching @ CS dpt

 - CNRS employees: contact the [CS department](mailto:direction-ufr@informatique.univ-paris-diderot.fr) if you'd like to teach
 - PhD students: first-timers have to [register](http://www.informatique.univ-paris-diderot.fr/ufr/doctorants_contractuels_mission_enseignement_2023_2024) by **Jun. 24**
 - ⚠️everyone: please keep Friday afternoons **free** to attend the meeting and seminar
---
# Free Software Development

 - at IRIF through software proposals for next September
 - CNRS support through the [OPEN program](https://www.cnrsinnovation.com/open/)
 - government [support scheme](https://code.gouv.fr/fr/utiliser/marches-interministeriels-support-expertise-logiciels-libres/)
 
---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### December 2, 2022

---
## Agenda

 - Dec. 8: [Exhibition](https://icalp2022.irif.fr/?page_id=1111) rerun _50 Years of Theoretical Computer Science_
 - Dec. 15: [Arnaud Sangnier's HDR](https://www.irif.fr/~sangnier/hdr.html) with talks by Nathalie Bertrand & Jérôme Leroux in the morning
 - Jan. 16–20: [CIRM Winter School](https://conferences.cirm-math.fr/2758.html) on discrete mathematics and logic; pre-register [*now*](https://www.cirm-math.fr/preRegistration/index.php?EX=menu0&id_renc=2758)
 
---
# HCERES

I will need volonteers to help with writing the `automata team' part of the report.

---
# Visitors

  Who will welcome visitors soon?

 - [Sarah Winter](https://sarahwinter.net/) Dec. 6–9
 - [Carl-Fredrik Nyberg Brodda](https://sites.google.com/view/cf-nb/?pli=1) Dec. 16
 - [George Metcalfe](https://www.math.unibe.ch/ueber_uns/personen/prof_dr_metcalfe_george/index_ger.html) and [Simon Santschi](https://www.math.unibe.ch/ueber_uns/personen/santschi_simon/index_ger.html) Jan. 23–28

---
## ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### October 20, 2023
---
## 💰 Funding

 - ![height:24px](https://sciencesmaths-paris.fr/images/tmpl/menu-logo.png) FSMP, deadline **Nov. 30**
   - [FSMP Chairs](https://www.sciencesmaths-paris.fr/en/?view=article&id=327)
   - [FSMP Postdocs](https://sciencesmaths-paris.fr/en/?view=article&id=326) and [Cofund Postdocs](https://www.mathingp.fr/)
 - [Eiffel scholarships](https://u-paris.fr/bourse-dexcellence-eiffel-lappel-a-candidatures-2024-est-ouvert/) for Masters and PhDs, deadline **Oct. 31**
---
## 💰 Funding

 - ![height:26px](https://anr.fr/typo3conf/ext/anr_skin/Resources/Public/assets/img/anr-logo-2021-complet.png) [ANR generic proposals](https://anr.fr/fr/detail/call/aapg-appel-a-projets-generique-2024/), deadline Oct. 19
 - ![height:36px](https://www.horizon-europe.gouv.fr/sites/default/files/2021-05/Logo_Horizon_Europe_1_2.png) [ERC starting grants](https://www.horizon-europe.gouv.fr/calendriers-et-documents-des-appels-erc-27857), deadline ~~Oct. 24~~ Nov. 7
 - ![height:36px](https://www.dagstuhl.de/_Resources/Static/Packages/Dagstuhl.Site/Frontend/images/LZI-Logo.jpg?bust=b988b355) [Dagstuhl Seminar Proposals](https://www.dagstuhl.de/en/seminars/dagstuhl-seminars/composition-of-a-proposal), deadline Nov. 1
 - ![height:36px](https://candidatures.iufrance.fr/files/iuf-candidatures/design/images/logo_2022.png) [IUF chairs](https://candidatures.iufrance.fr/accueil.html), deadline Nov. 6
 
---
## 📅 Upcoming Deadlines

 - [PODS 2024](https://2024.sigmod.org/calls_papers_pods_research.shtml), Santiago, Chile, deadline Dec. 4
 - [ESSLLI 2024](https://2024.esslli.eu/), Leuven, Belgium, deadline for course titles Dec. 15 and course proposals Jan. 12
 - [LICS 2024](https://lics.siglog.org/lics24/), Talinn, Estonia, deadline Jan. 21
 - [ICALP 2024](https://compose.ioc.ee/icalp2024/), Talinn, Estonia, deadline Feb. 14
---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
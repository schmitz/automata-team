---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### October 14, 2022

---
## Team Life

 - **Pole day** next Friday Oct. 21
 - newcomer: [Florian Renkin](https://www.lrde.epita.fr/~frenkin/) as ATER on Nov. 1
---
## Reminder: Sabbaticals

 - [CRCT CNU](https://cnu27.univ-lille.fr/crct.html): submit on [ANTARES](https://galaxie.enseignementsup-recherche.gouv.fr/antares/ech/index.jsp) by **Oct. 18**
 - CNRS delegation: submit on [SIRAH](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_acc_delegation_CNRS.htm) by **Oct. 18**
 
   Especially for [International Research Labs](https://www.ins2i.cnrs.fr/fr/international)
---
## Funding

 - [IdEx Émergence](https://u-paris.fr/appel-emergence-en-recherche-2022/) by Dec. 15, 11:00
   - ⚠ submit both `.pdf` and `.doc/.odt/.docx`
   - 📅 1–2 years
   - 💰 k€10–40 total funding
   - 🧑‍🏫 teaching waiver up to a total of k€5≈80hrs
 * also soon: CNRS call                                         .
---
## Upcoming Conference Deadlines

 - [VLDB 2023](https://vldb.org/2023/), Vancouver, every 20th until March
 - [PODS 2023](https://2023.sigmod.org/calls_papers_pods_research.shtml), Seattle, deadline Nov. 28
 - [LICS 2023](https://lics.siglog.org/lics23/), Boston, deadline Jan. 18
 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, deadline Feb. 11
 - [KR 2023](https://kr.org/KR2023/), Rhodes, deadline Mar. 7
 

---
## HCERES

 - please check your work is referenced on the [HAL IRIF](https://hal.archives-ouvertes.fr/IRIF)
 - who would like to participate to a how-to tutorial session?
---
## ?

 - who will welcome visitors soon?
 - anyone would like to give a talk?
 - what about the last conference(s) you attended?
 - any paper(s) in preparation?
 - start a reading group on a topic?
 - internship proposals to MPRI/LMFI students?
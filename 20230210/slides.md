---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### February 10, 2023

---
## Upcoming Conference Deadlines

 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, deadline Feb. 11
 - [DLT 2023](https://dltwords2023.cs.umu.se/dlt), Umeå, deadline Feb. 19
 - [Words 2023](https://dltwords2023.cs.umu.se), Umeå, deadline Feb. 26
 - [VLDB 2023](https://vldb.org/2023/), Vancouver, every 20th until March
 - [KR 2023](https://kr.org/KR2023/), Rhodes, deadline Mar. 7

---
## Upcoming Conferences

 - [CSL 2023](https://csl2023.mimuw.edu.pl/), Warsaw, Feb. 13–16
 - [STACS 2023](https://www.conferences.uni-hamburg.de/event/272/), Hamburg, Mar. 7–10 
 - [ETAPS 2023](https://etaps.org/2023/), Paris, Apr. 22–27
 
---
# Visitors

  Who will welcome visitors soon?

 - [Dong Han Kim](https://sites.google.com/site/drkimdh/) Feb. 6–15
 - [Rémi Morvan](https://www.morvan.xyz/) Feb. 13–14
 - [León Bohn](https://leonbohn.de/) Mar. 27–31

---
# HCERES

## General data

 - [current data](https://cloud.irif.fr/s/6D2ajWqXmarskTQ): to check
 - [software & lecture notes](https://docs.google.com/spreadsheets/d/1R4mG7EEDT80HsLyt54Im6UWOUi5Su7klefZC1Hq-Z6Y/edit#gid=0): to complete

---
# HCERES 

## Bibliography

 - full `automata` bibliography sent by Thomas
 - [team associated with each entry](https://lite.framacalc.org/publications-irif-equipes-9z74): to disambiguate
 - [spurious publications](https://lite.framacalc.org/publications-irif-equipes-9z74): to remove

---
# HCERES

 - LaTeX template online on [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres) and sent by email
   * recommended: activate your [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres) account
   * todo now: sec. 1.1 (`info-eqth.tex`)
     - either commit changes to [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres)
     - or send to Thomas and me by email

---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### March 10, 2023

---
## ![w:48](https://zulip.irif.fr/static/images/favicon.svg?v=4) Chat server

 - [zulip server](https://zulip.irif.fr/)
 - now hosted at IRIF
 - TODO: activate account by connecting (same login/password as the other services at IRIF)

---
## 🚧 Construction work at IRIF

 - [planned work](https://www.irif.fr/intranet/travaux)
 - noisy environment from April to September
 - [meeting rooms 146 & 147 at Olympe de Gouges](https://reservation.irif.fr/)

---
##  Request from INS2I

 - list research in connection with ecological concerns
 - old, current, or emerging
 - send email to [Giuseppe Castagna](mailto:Giuseppe.Castagna@irif.fr)

---
# ![h:50](https://u-paris.fr/wp-content/uploads/2022/03/Universite_Paris-Cite-logo.jpeg) Invited Prof Program

Invited professorships at U. Paris Cité
 - [submit a proposal](https://sondage.app.u-paris.fr/653794?lang=fr)
 - 📅 deadline **Mar. 20**

---
## ![h:40](https://sciencesmaths-paris.fr/images/tmpl/menu-logo.png) Funding for Masters

 - from Fondation Sciences Mathématiques de Paris 
 - 1 or 2 years
 - deadline **May 9, 2023**
 - additional funding for women thanks to IRIF
 - [link](https://sciencesmaths-paris.fr/en/nos-programmes-en/pgsm-master)

---
## ![h:50](https://u-paris.fr/wp-content/uploads/2022/03/Universite_Paris-Cite-logo.jpeg) Funding for events

 - endowed by Faculté des Sciences of U. Paris Cité
 - specific funding for GDR-sponsored events
 - event venue must be at U. Paris Cité
 - 💰 up to K€2.5
 - form to send to [Émilie Boutin](mailto:emilie.boutin@u-paris.fr)

---
## 📅 Upcoming Deadlines

 - [VLDB 2023](https://vldb.org/2023/), Vancouver, deadline Mar. 20
 - [ICDT 2024](https://dastlab.github.io/edbticdt2024/), Paestum, first round  deadline Mar. 20
 - [Concur 2023](https://www.uantwerpen.be/en/conferences/confest-2023/), Antwerp, deadline Apr. 24
 - [MFCS 2023](https://mfcs2023.labri.fr/), Bordeaux, deadline Apr. 24

---
## 📅 Upcoming Conferences

 - [ICDT 2023](http://edbticdt2023.cs.uoi.gr/), Iannina, Mar. 28–31
 - [JNIM 2023](https://jnim2023.sciencesconf.org/), Paris, Apr. 4–7
 - GT DAAL, Kremlin-Bicêtre, Apr. 21
 - [ETAPS 2023](https://etaps.org/2023/), Paris, Apr. 22–27

---
## Upcoming: JNIM 2023

### journées nationales du [GDR IM](https://www.gdr-im.fr/)

 - 📅 Apr. 4–7 at Grands Moulins
 - [program](https://jnim2023.sciencesconf.org/)
 - [registration](https://jnim2023.sciencesconf.org/registration)
 
---
## Upcoming: [GT DAAL](https://daal.labri.fr/)
 - 📅 Apr. 21 (right before ETAPS)
 - 🎓 invited speakers:
   - [Florent Koechlin](https://igm.univ-mlv.fr/~koechlin/)
   - [Liat Peterfreund](https://sites.google.com/view/liatpeterfreund/)
   - [Marie Van Den Bogaard](https://pagespro.univ-gustave-eiffel.fr/marie-van-den-bogaard)
   - [Noam Zeilberger](http://noamz.org/)
 - 📢 call for presentations:
   - deadline **yesterday**
   - by email uli@lrde.epita.fr
  
---
# HCERES

## General data

 - [current data](https://cloud.irif.fr/s/6D2ajWqXmarskTQ): to check

---
# HCERES 

## Bibliography

**⚠️ LAST DAY TODAY**

 - [team associated with each entry](https://lite.framacalc.org/publications-irif-equipes-9z74): to disambiguate
 - [spurious publications](https://lite.framacalc.org/publications-irif-equipes-9z74): to remove
 - please send missing bibliographic entries

---
# HCERES

**⚠️ BY NEXT WEEK**
 - c.f. email sent by Thomas
   * LaTeX template online on [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres)
   * or by email to Thomas & me

---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### March 17, 2023

---
## ![w:48](https://zulip.irif.fr/static/images/favicon.svg?v=4) Chat server

 - [zulip server](https://zulip.irif.fr/)
 - now hosted at IRIF
 - [HCERES stream](https://zulip.irif.fr/#narrow/stream/85-Team-automata-HCERES)
 - TODO: activate account by connecting (same login/password as the other services at IRIF)

---
## 📅 ASD Seminar

 - Wed., Mar. 22 at 3pm
 - Michael Lampis
 - [First Order Logic on Pathwidth Revisited Again](https://arxiv.org/abs/2210.09899)

---
## GDR-IM

 - [groupement de recherche en informatique mathématique](https://www.gdr-im.fr/)
 - TODO: [register](https://mygdr.hosted.lip6.fr/register) if not already done
 
---
## 📅 Upcoming Deadlines

 - [VLDB 2023](https://vldb.org/2023/), Vancouver, deadline Mar. 20
 - [ICDT 2024](https://dastlab.github.io/edbticdt2024/), Paestum, first round  deadline Mar. 20
 - [Concur 2023](https://www.uantwerpen.be/en/conferences/confest-2023/), Antwerp, deadline Apr. 24
 - [MFCS 2023](https://mfcs2023.labri.fr/), Bordeaux, deadline Apr. 24

---
## 📅 Upcoming Conferences

 - [ICDT 2023](http://edbticdt2023.cs.uoi.gr/), Iannina, Mar. 28–31
 - [JNIM 2023](https://jnim2023.sciencesconf.org/), Paris, Apr. 4–7
 - GT DAAL, Kremlin-Bicêtre, Apr. 21
 - [ETAPS 2023](https://etaps.org/2023/), Paris, Apr. 22–27

---
# HCERES

 - [current data](https://cloud.irif.fr/s/6D2ajWqXmarskTQ)
 - [git repository](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres)

---
# HCERES

 - main activities: send your summary to Thomas
 - portfolio proposal: picture + short texts for:
   - Handbook of automata
   - one salient result in DB theory
   - one salient result in combinatorics on words
   - Gaëtan Doueneau's ICALP'22 paper
   - the Highlights conference

---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### November 25, 2022

---
## Lab Life

 - [Exhibition](https://icalp2022.irif.fr/?page_id=1111) rerun _50 Years of Theoretical Computer Science_ on Dec. 8 noon at the library; register with Sandrine Cadet by Dec. 2
 - University's [newcomers day](https://cloud.parisdescartes.fr/index.php/s/tFodpXjFPzHrt9e) on Jan. 17; register by Jan. 6
 
---
# M2 Internships

 Time to offer M2 internships!
 
 Contact points with courses at [MPRI](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=start) and [LMFI](https://master.math.univ-paris-diderot.fr/annee/m2-lmfi/):
 - [Thomas Colcombet](https://master.math.univ-paris-diderot.fr/modules/m2lmfi-second-ordre/)
 - [Daniela Petrisan, Matthieu Picantin, Amaury Pouly, and Sam van Gool](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-16)

Who has been in contact with students?

---
# Émergence Funding

 - [IdEx Émergence](https://u-paris.fr/appel-emergence-en-recherche-2022/) by **Dec. 15**, 11:00
   - ⚠ submit both `.pdf` and `.doc/.odt/.docx`
   - 📅 1–2 years
   - 💰 k€10–40 total funding
   - 🧑‍🏫 teaching waiver up to a total of k€5≈80hrs

Who will apply?

---
# Postdoc Funding

 - [FSMP Postdoc](https://sciencesmaths-paris.fr/en/nos-programmes-en/postdocs) & [Cofund](https://www.mathingp.fr/): by **Nov. 30**

Who has been in contact with prospective applicants?



---
# Visitors

  Who will welcome visitors soon?

 - [Sarah Winter](https://sarahwinter.net/) Dec. 6–9
 - [Carl-Fredrik Nyberg Brodda](https://sites.google.com/view/cf-nb/?pli=1) Dec. 16

---
## ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?
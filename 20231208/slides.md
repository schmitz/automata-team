---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### December 8, 2023

---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Visit

 - Thank you for your participation!
 - Team slides are [online](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/raw/master/hceres/slides/HCERES-eq-automates-v1.pdf?inline=false)

---
## Internship Proposals

 - [@ ENS Paris & Paris-Saclay](https://stages.di.ens.fr/internships/submit)
 - [Bachelor @ ENS Lyon](http://perso.ens-lyon.fr/michele.pagani/soumission_l3internships.html)
 - [M2 @ MPRI](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=internships)
 - centralise and publish internship proposals on the web?

---
## 📅 Upcoming Deadlines

 - [ESSLLI 2024](https://2024.esslli.eu/), Leuven, Belgium, deadline for course titles Dec. 15 and course proposals Jan. 12
 - [LICS 2024](https://lics.siglog.org/lics24/), Talinn, Estonia, deadline Jan. 21
 - [ICALP 2024](https://compose.ioc.ee/icalp2024/), Talinn, Estonia, deadline Feb. 14
 
---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
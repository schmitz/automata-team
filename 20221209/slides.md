---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### December 9, 2022

---
# Lab life

 - [call for volunteers](https://www.irif.fr/intranet/lettre-irif/2022-11-25/index) to participate to the future commission for the adaptation to environmental challenges
 - winter closure: [request form](https://sdstm.math-info-paris.cnrs.fr/shared/EI97a2hL7mnYXWdplm5cldmUscph1DRD9TXNi2nwtoc) for accessing the lab between Dec. 24 and Jan. 1

---
# Émergence Funding

 - [IdEx Émergence](https://u-paris.fr/appel-emergence-en-recherche-2022/) by **Dec. 15**, 11:00
   - ⚠ submit both `.pdf` and `.doc/.odt/.docx`
   - 📅 1–2 years
   - 💰 k€10–40 total funding
   - 🧑‍🏫 teaching waiver up to a total of k€5≈80hrs

→ need a letter of support from the [IRIF direction](mailto:direction@irif.fr)

---
# Agenda

 - Dec. 13: SIF day ["autour du doctorat"](https://www.societe-informatique-de-france.fr/les-journees-sif/journee-autour-du-doctorat-2022/); [registration](https://framaforms.org/journee-autour-du-doctorat-2022-1668605192) to watch in room 3052
 - Dec. 15: [Arnaud Sangnier's HDR](https://www.irif.fr/~sangnier/hdr.html) with talks by Nathalie Bertrand & Jérôme Leroux in the morning
 - Dec. 16: IRIF end-of-year get together at lunch on 3rd floor; **no team meeting**
 - Jan. 16–20: [CIRM Winter School](https://conferences.cirm-math.fr/2758.html) on discrete mathematics and logic; pre-register [*now*](https://www.cirm-math.fr/preRegistration/index.php?EX=menu0&id_renc=2758)
 ---
## Upcoming Conference Deadlines

 - [VLDB 2023](https://vldb.org/2023/), Vancouver, every 20th until March
 - [LICS 2023](https://lics.siglog.org/lics23/), Boston, deadline Jan. 18
 - [FSCD 2023](https://easyconferences.eu/fscd2023/), Rome, deadline Jan. 30
 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, deadline Feb. 11
 - [KR 2023](https://kr.org/KR2023/), Rhodes, deadline Mar. 7

---
## Upcoming Conferences

 - [POPL 2023](https://popl23.sigplan.org/), Boston, Jan. 15–21
 - [CSL 2023](https://csl2023.mimuw.edu.pl/), Warsaw, Feb. 13–16
 - [STACS 2023](https://www.conferences.uni-hamburg.de/event/272/), Hamburg, Mar. 7–10 
 - [ETAPS 2023](https://etaps.org/2023/), Paris, Apr. 22–27

Anyone planning on attending?  Give a short report on the conference for the team?
 
---
# Visitors

  Who will welcome visitors soon?

 - [Carl-Fredrik Nyberg Brodda](https://sites.google.com/view/cf-nb/?pli=1) Dec. 16
 - [George Metcalfe](https://www.math.unibe.ch/ueber_uns/personen/prof_dr_metcalfe_george/index_ger.html) and [Simon Santschi](https://www.math.unibe.ch/ueber_uns/personen/santschi_simon/index_ger.html) Jan. 23–28

---
## ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?
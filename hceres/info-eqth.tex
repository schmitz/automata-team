%!TEX root =  ./main.tex

Les membres de l'équipe \automates\ appartiennent également pour
moitié à d'autres équipes, couvrant les trois autres pôles du
laboratoire. Il s'agit de l'équipe \verif\ pour
Irène \bsc{Guessarian}, Peter \bsc{Habermehl}, Florian \bsc{Horn} et
Sylvain \bsc{Schmitz}, l'équipe \combi\ pour Valérie \bsc{Berthé} et
Wolfgang \bsc{Steiner}, et l'équipe \algebre\ pour Sam \bsc{van Gool},
Paul-André \bsc{Melliès}, Daniela \bsc{Petri\c san} et
Matthieu \bsc{Picantin}. D'autres membres entretiennent des liens
forts avec ces équipes, sans que cela soit nécessairement formalisé,
pour des raisons historiques et le fait que l'on conseille aux
nouveaux membres de rang~B de n'appartenir qu'à une équipe. Ces
connexions diverses avec le reste du laboratoire sont l'une des
spécificités de l'équipe \automates.

\section{Les thématiques scientifiques et leurs enjeux} \label{automates:thm}

\hceres{Il s’agit de la section ciblée « Science » de
  l’équipe. Présenter les thématiques de recherche abordées au cours
  de la période de référence, en les situant dans le contexte
  international. Les principaux résultats scientifiques de l’équipe
  sont présentés. \textbf{Ce texte pourra être substantiel tout en restant
  d’une longueur raisonnable et adapté à la taille de l’unité de
  recherche.} Il trouvera un écho dans le profil d’activités ainsi que
  dans le portfolio. Cette description sera projetée dans
  l'auto-évaluation selon le référentiel. Elle sera brièvement reprise,
  au titre de contexte, dans la trajectoire de l’équipe.}

\paragraph{Thématiques scientifiques}
\newcommand\autoemph[1]{\textbf{#1}}
\def\ignorecite#1{}

\hceresbis{Présentation en détail des thématiques abordées sur la période
  et enjeux}

Le concept d'\autoemph{automate} correspond à un modèle de calcul
simple, le plus souvent fini, au pouvoir d'expressivité relativement
faible.  Cette apparente simplicité en fait un objet particulièrement
riche, à la fois dans son étude, dans ses applications, et dans ses
liens avec d'autres thèmes de l'informatique et des
mathématiques. Nous organisons ces liens autour de quatres grande
interactions: la logique, la combinatoire, l'algèbre et les modèles
de calcul. 

\noindent\textit{Automates et logique :} Les automates entretiennent un
lien fort avec la logique. En effet, ils peuvent d'une part être vus
comme des classes de formules particulières, tout comme servir de
cible pour la compilation de formules. Cette connexion est
intensivement utilisée, entre autres, dans les algorithmes de
vérification et de synthèse. Plus généralement, les automates jouent
un rôle clef en théorie algorithmique des modèles, c'est-à-dire dans
l'étude de l'effectivité et de la complexité de problèmes de décision
exprimés par des formules logique, faisant référence à des structures
finies ou finiment représentées. En particulier, les questions
sémantiques et algorithmiques associées aux bases de données sont de
ce type. Motivés par les applications en vérification, dans lesquelles
la logique est le formalisme majeur pour exprimer les spécification,
les automates ont été étendus pour traiter d'objets infinis, comme des
mots ou des arbres, qui sont des modèles naturels de la notion de
temps. Ces généralisations nécessitent de tisser des liens étroits
avec la théorie des jeux sur les graphes ainsi qu'avec des structures
mathématiques comme les ordinaux ou la théorie des ensembles.

\noindent\textit{Automates et combinatoire :} Le lien le plus évident
entre les automates et la combinatoire se trouve en combinatoire des
mots. Les automates sont également utilisés dans la compréhension
d'objets mathématiques complexes. C'est le cas dans l'étude des
systèmes dynamiques, dont l'évolution peut se décrire au moyen de la
théorie des langages. Pour des raison similaires, ils s'avèrent
pertinents dans l'étude de la notion d'aléatoire. Les systèmes de
numération, qui modélisent les différentes façons de représenter les
nombres, font aussi un usage important des automates ; les opération
élémentaires -- comme l'addition -- pouvant se représenter à l'aide de
machines à état fini. 
Les automates servent également à coder symboliquement des objets
complexes infinis, comme des groupes ou d'autres structures
mathématiques. Cette approche s'avère féconde pour construire des
témoins d'existence de structures algébriques exhibant des propriétés
combinatoires ou asymptotiques complexes.

\noindent\textit{Automates et algèbre :} Un autre aspect essentiel des
automates est le lien profond qu'ils entretiennent avec l'algèbre. Ce
lien a été mis en évidence très tôt, avec l'étude des demi-groupes
finis et leur usage dans la caractérisation et la compréhension de
classes de langages réguliers. Mais cette relation se décline
également au travers d'automates à la sémantique étendue par des
pondérations dans des semi-anneaux, ou plus généralement définis sur
des catégories aux bonnes propriétés. Cette approche permet
aujourd'hui de dresser des ponts avec la sémantique des langages. La
topologie, et plus particulièrement la dualité de \bsc{Stone}, s'avère
également fructueuse dans la modélisation de la finitude des
automates.

\noindent\textit{Automate comme modèle de calcul :} Les automates
pouvant être vus comme un modèle de calcul particulièrement simple,
leur étude participe de l'analyse de classes de faible complexité ou
d'algorithmes particuliers. Ces connexions sont fortes avec les
classes de circuits, qu'ils soient booléens ou algébriques. Les
automates cellulaires, les automates distribués sur les graphes, les
systèmes d'additions de vecteurs ou les automates d'ordre supérieur,
sont également des modèles possédant à la fois des états finis
permettant leur étude sous l'angle de la théorie des automates, et une
sémantique exhibant plus de complexité calculatoire. Leur étude entre
donc naturellement dans les thèmes de l'équipe.


\paragraph{Positionnement scientifique par rapport au contexte international}

\hceresbis{\textbf{Ce point est souligné comme important par l’HCERES.} Donner des indicateurs : conférences, rapports, appels à projets, ou encore keynote, prix, défis…}

L'équipe automates aborde les automates sous une variété
d'angles. Elle est en se sens singulière dans le contexte
international et possède une expertise unique. L'équipe automates est
intégrée à plusieurs communautés, et elle l'est au plus haut niveau
dans chacune d'entre elles.

Ce fait s'observe tout d'abord dans ses publications dans les
meilleurs journaux et les meilleures conférences, régulièrement
récompensées par des prix (\emph{ICALP Best Paper Award}
2020, \emph{EACSL Helena \bsc{Rasiowa} Award} 2022, \emph{ICALP Best
Student Paper Award} 2022, \emph{MFCS Best Student Paper Award}
2022). Les membres de l'équipe sont régulièrement invités à donner des
exposés dans les conférences les plus prestigieuses du domaine
(LICS~2017 et 2019, FoSSaCS~2019, CSL~2021). Les recherches menées
dans l'équipe ont aussi été distinguées à l'%% au niveau national
%% (prix \emph{L'Oréal--Unesco pour les femmes et la science}, prix de
%% thèse Gilles \bsc{Kahn} et de la Chancellerie de Paris) et
international (prix Arto \bsc{Salomaa}, \emph{IFIP Fellow}, membre
étranger de l'Académie des Sciences d'Arménie).

Le positionnement de l'équipe dans un contexte international
transparaît aussi dans sa participation à des projets européens (une
ERC, un financement Marie \bsc{Curie}, une ANR+FWF avec l'Autriche), à
des sociétés savantes (EATCS Council, IFIP), à des comités de pilotage
(STACS, ICALP, Highlights, LATIN Words) et comités éditoriaux
(\emph{Fund. Inform.}, \emph{Inform. Proc. Letters}, \emph{RAIRO
ITA}).

Enfin, les membres du laboratoire ont construit de solides
collaborations internationales, en Europe (Allemagne, Autriche,
Italie, Pologne, Portugal, Royaume-Uni, Suisse), et au-delà
(Argentine, Canada, Chili, Corée du Sud, États-Unis, Israël, Japon).

\paragraph{Avancées scientifiques majeures dans la période}

\hceresbis{On cite les travaux qui apparaîtront dans un bibtex en annexe comme sélection de publications. Viser 20\% comme dans le précédent rapport HCERES.}


%\textcolor{red}{TODO À faire: En attente de la dernière modif de Wolfgang.}


Une sélection d'avancées scientifiques remarquables sont présentées
dans cette section, organisée selon un découpage qui correspond
essentiellement aux interactions de l'équipe \automates\ avec les
autres équipes du laboratoire.

\subparagraph*{Fondements algébriques et topologiques de la théorie des automates.}
Beaucoup de résultats profonds en théorie des automates s'expriment
comme des descriptions effectives de classes de langages réguliers,
appelées \emph{variétés de langages}, au moyen d'identités profinies
(il s'agit d'identités entre des suites de termes convergentes dans la
topologie profinie) satisfaites par un objet algébrique
caractéristique du langage, comme le monoïde syntaxique. L'équipe est
historiquement, et continue d'être, à la pointe dans l'analyse de ces
caractérisations, comme dans \cite{DBLP:journals/actaC/AlmeidaEP17}
pour les variétés positives, ou le survol des hiérarchies de
différences \cite{DBLP:journals/lmcs/CartonPP18}.

Plus récemment, un intense travail a été fourni afin de comprendre ce
que sont ces identités profinies et comment les généraliser. Une
réponse est que la correspondance entre variétés de monoïdes et
variétés de langages est un cas particulier d'une construction
classique en topologie: la dualité de \bsc{Stone}. Le travail a
consisté à comprendre comment utiliser cette approche au delà des
langages réguliers et à quoi correspondent les constructions
classiques dans ce cadre, comme par exemple
dans \cite{DBLP:conf/lics/GehrkePR17} pour l'étude des
quantificateurs. Une approche logique aux mots profinis au moyen de
modèles saturés a été également considérée~\cite{MR4040833}.

Dans une autre direction, les outils algébriques permettent de mettre
en évidence le pourquoi des remarquables propriétés des automates,
comme l'existence d'automates ou de demi-groupes canoniques, la
possibilité de faire de l'apprentissage, ou de décider du vide. Ici
les catégories offrent un cadre idéal, et ont permis de revisiter et
d'étendre les résultats de minimisation et d'apprentissage
d'automates \cite{DBLP:conf/csl/ColcombetPS21}, ou encore de
comprendre comment traiter l'épineux problème de l'interaction entre
le non-déterminisme et les probabilités \cite{DBLP:conf/lics/0003P20}.
Une autre extension naturelle est de passer des mots aux arbres. Là
encore plusieurs résultats significatifs ont été obtenus, comme dans
l'étude des congruences des algèbres libres d'arbres \cite{MR4169368},
et l'expressivité des langages réguliers d'arbres reconnus par les
algèbres finies sous contrainte de
cardinalité \cite{DBLP:conf/mfcs/ColcombetJ22}.

Enfin, un dernier lien avec l'algèbre est une application de la
théorie des automates pour représenter des objets infinis, comme des
structures algébriques. Les demi-groupes infinis ont été étudiés dans
l'équipe sous cet angle : un demi-groupe est automatique si ses
éléments sont des mots, et le produit avec un des générateurs peut
être représenté par un
automate \cite{DBLP:conf/wia/BartholdiGKP18,DBLP:journals/ijac/DAngeliGKPR20}.


\subparagraph*{Formes généralisées d'automates pour la vérification.}
Dans de nombreux contextes liés à la vérification, les automates finis
simples ne suffisent plus, et il est nécessaire d'aller au delà. Les
possibilités sont multiples et motivées par des applications très
différentes : calculer des valeurs, produire des mots en sorties, ou
considérer des entrées plus complexes que les mots finis, par
exemple.

Une première généralisation clef est celle d'automate quantitatif,
c'est-à-dire représentant une fonction des mots dans, par exemple, un
corps, ou plus généralement, un semi-anneau. Ces objets peuvent servir
à analyser les boucles de programmes, ou modéliser la consommation
d'un circuit, par exemple. L'algorithmique et l'implémentation de ces
automates est étudiée finement dans l'équipe (voir par
exemple \cite{DBLP:journals/fuin/LombardyS22}). Ces travaux
nourrissent l'implémentation de la
plate-forme \href{http://vaucanson-project.org/Awali/2.2/}{Awali} qui
permet de réaliser des calculs efficacement sur ces objets dans leur
forme générale (voir section~\ref{autoev:dom4:ref2}). 

Les transducteurs sont des automates décrivant des fonctions des mots
dans les mots, et sont omniprésents en théorie des langages (ils
permettent de réduire un langage à un autre par exemple) ou en
vérification (ils modélisent par exemple des transformateurs de
documents ou des spécifications reliant les entrées et les sorties
d'un système informatique).  L'équipe a participé à plusieurs avancées
dans la compréhension des propriétés subtiles des fonctions calculées
par transducteurs, comme pour l'étude de la continuité de ces
fonctions \cite{DBLP:journals/lmcs/CadilhacCP19}, ou la
caractérisation de sous-classes d'expressivité réduite, qui est
présentée dans l'\hyperref[automates:portfolio4]{entrée 4 du
portfolio} \cite{DBLP:conf/icalp/Doueneau-Tabot22}.

Une autre généralisation des automates essentielle en vérification
est celle vers les mots et les arbres infinis. En effet, certains
systèmes à étudier ou à synthétiser interagissant avec
l'environnement, et sont susceptibles de s'exécuter pendant un temps
infini. Le déroulement du temps peut alors être vu comme un mot infini
(ce que l'on appelle le temps linéaire) ou comme un arbre infini (ce
que l'on appelle le temps branchant, représentant plusieurs avenirs
possibles). Sur les mots infinis, la théorie se rapproche
progressivement des résultats les plus pointus sur les mots finis,
comme pour les questions de
séparation~\cite{DBLP:conf/fossacs/ColcombetGM22}.

Dans ces études, un autre objet joue un rôle absolument crucial : les
jeux de durées infinies, et de tels jeux servent de manière plus
générale en vérification, car de nombreux problèmes de décision se
réduisent à déterminer le vainqueur dans un jeu, et à calculer la
stratégie gagnante correspondante. L'équipe est à l'origine d'avancées
importantes dans leur étude, dans l'algorithmique de la résolution des
jeux finis de différentes
natures \cite{DBLP:journals/lmcs/ColcombetFGO22,DBLP:conf/lics/ColcombetJLS17}.


\subparagraph*{Automates et logique.}
Les formalismes logiques se retrouvent partout dans l'étude des
automates, et sont présents dans de nombreuses contributions de
l'équipe -- par exemple en relation avec la vérification. Certains
travaux s'intéressent néanmoins plus spécifiquement à l'expressivité
de formalismes logiques, et aux questions de décidabilité de
complexité correspondantes : il s'agit de la théorie algorithmique des
modèles.

Les bases de données en sont un exemple essentiel. Dans cette
direction, une étude approfondie de la sémantique des base de données
incomplètes a été menée dans l'équipe et est présentée dans
le \hyperref[automates:portfolio2]{second item} du portfolio
(section~\ref{automates:portfolio2}), et s'est poursuivie par
l'identification de classes de requêtes pour les données
inconsistantes auxquelles il est possible de répondre
efficacement~\cite{DBLP:conf/datalog/GheerbrantLRS22}.

Par ailleurs, des travaux en lien plus direct avec la théorie des
modèles classique sont également poursuivis dans l'équipe, sur les
théorèmes de préservation pour la logique du premier
ordre \cite{DBLP:conf/lics/Lopez22}, sur des extensions de cette
logique \cite{MR4357456}, ainsi que la théorie de certaines structures
infinies remarquables~\cite{DBLP:journals/fuin/BesC22}. Enfin, les
beaux pré-ordres, objets clefs en théorie des modèles ainsi que dans la
compréhension de la terminaison et de la complexité de plusieurs
algorithmes en automates et en vérification, sont également finement
étudiés au sein l'équipe \cite{MR4291877}.

\subparagraph*{Automates et combinatoire.}
L'équipe \automates\ est très active dans les nombreux sujets relevant
des automates et de la combinatoire. Un premier est celui de la
régularité dans l'apparition des facteurs dans une séquence, sujet
classique en combinatoire des mots. L'équipe a développé en
particulier le notion de mots dendriques, qui étend les notions
classiques de mots sturmiens, les codages d'intervalles et de mots
d'\bsc{Arnoux}-\bsc{Rauzy} \cite{DBLP:journals/tcs/BertheB19,MR4228544}.
%Combinatoire des mots:
\ignorecite{MR3628916,% Valérie \bsc{Berthé}, Vincent Delecroix, Francesco Dolce, Dominique Perrin, Christophe Reutenauer, and Giuseppina Rindone. “Return words of linear involutions and fundamental groups”. In: Ergodic Theory Dynam. Systems 37.3 (2017). Équipes automates, combi, pp. 693–715.
DBLP:journals/ijfcs/MasseBRBDD0P18a,% Valérie \bsc{Berthé}, Francesco Dolce, Fabien Durand, Julien Leroy, and Dominique Perrin. “Rigidity and Substitutive Dendric Words”. In: Int. J. Found. Comput. Sci. 29.5 (2018). Équipes automates, combi, pp. 705–720.
DBLP:journals/lmcs/CartonB20,% Luc Boasson and Olivier \bsc{Carton}. “Transfinite Lyndon words”. In: Log. Methods Comput. Sci. 16.4 (2020). Équipe automates, Paper No. 9, 38.
MR4228544,% Valérie \bsc{Berthé}, Paulina Cecchi Bernales, Fabien Durand, Julien Leroy, Dominique Perrin, and S. Pe- tite. “On the dimension group of unimodular S-adic subshifts”. In: Monatsh. Math. 194.4 (2021). Équipes automates, combi, pp. 687–717.
DBLP:journals/tcs/BertheB19,% Valérie \bsc{Berthé} and Paulina Cecchi Bernales. “Balancedness and coboundaries in symbolic systems”. In: Theor. Comput. Sci. 777 (2019). Équipes automates, combi, pp. 93–110.
DBLP:conf/cwords/Lapointe21,% Mélodie Lapointe. “Perfectly Clustering Words are Primitive Positive Elements of the Free Group”. In: Combinatorics on words. Ed. by Thierry Lecroq and Svetlana Puzynina. Vol. 12847. Lect. Notes Comput. Sci. Équipe automates. Springer, 2021, pp. 117–128.
MR4405998,% Sébastien Labbé and Mélodie Lapointe. “The q-analog of the Markoff injectivity conjecture over the lan- guage of a balanced sequence”. In: Comb. Theory 2.1 (2022). Équipe automates, Paper No. 9, 25.
DBLP:journals/tcs/DolceRR19,% Francesco Dolce, Antonio Restivo, and Christophe Reutenauer. “On generalized Lyndon words”. In: Theor. Comput. Sci. 777 (2019). Équipes automates, combi, pp. 232–242.
DBLP:conf/csr/DolceP19,% Francesco Dolce and Dominique Perrin. “Eventually Dendric Shifts”. In: Computer science—theory and applications. Ed. by René van Bevern and Gregory Kucherov. Vol. 11532. Lect. Notes Comput. Sci. Équipes automates, combi. Springer, 2019, pp. 106–118.
DBLP:conf/cwords/DolceP19,% Francesco Dolce and Dominique Perrin. “Return Words and Bifix Codes in Eventually Dendric Sets”. In: Combinatorics on words. Ed. by Robert Mercas and Daniel Reidenbach. Vol. 11682. Lect. Notes Comput. Sci. Équipes automates, combi. Springer, 2019, pp. 167–179.
DBLP:journals/ijac/DolcePRRR18,% Francesco Dolce, Dominique Perrin, Antonio Restivo, Christophe Reutenauer, and Giuseppina Rindone. “Birecurrent sets”. In: Int. J. Algebra Comput. 28.4 (2018). Équipes automates, combi, pp. 613–652.
DBLP:conf/cpm/DolceRR19,% Francesco Dolce, Antonio Restivo, and Christophe Reutenauer. “Some Variations on Lyndon Words”. In: 30th Annual Symposium on Combinatorial Pattern Matching, CPM 2019, June 18-20, 2019, Pisa, Italy. Ed. by Nadia Pisanti and Solon P. Pissis. Vol. 128. LIPIcs. Leibniz Int. Proc. Inform. Équipes automates, combi. Schloss Dagstuhl. Leibniz-Zent. Inform., Wadern, 2019, 2:1–2:14.
MR4281387% Mélodie Lapointe and Christophe Reutenauer. “On the Frobenius conjecture”. In: Integers 21 (2021). Équipe automates, Paper No. A67, 9.
}
Ces études sont également en lien avec la dynamique symbolique, qui
est l'étude géométrique et statistique des trajectoires obtenues par
itération d'une operation sur un espace fixé.
L'\hyperref[automates:portfolio3]{item 3} du portfolio
présente une avancée importante de l'équipe concernant une
généralisation des mots sturmiens comme codages symboliques de
transformations agissant sur des tores.
D'autres travaux~\cite{MR3986918,MR4015135}, motivés par
la conjecture de \bsc{Pisot}, s'attachent à décrire symboliquement
(presque) toutes les translations du tore.

Un autre pont avec la combinatoire concerne les systèmes de
numération.  En effet, les nombres peuvent être représentés par des
séquences de symboles dans différents systèmes de numération, et les
opérations qui leur sont appliquées peuvent encodées et étudiées au
moyen d'automates. Les systèmes dynamiques apparaissent naturellement
dans ce contexte, soit sous forme symbolique, soit comme moyen de
décrire ces systèmes de numération (avec en particulier la
$\beta$-numération).  La propagation de retenue -- c'est-à-dire le
nombre de chiffres qui sont modifiés lorsqu'un nombre est incrémenté
-- est ainsi étudiée sous cet angle
dans~\cite{DBLP:journals/aam/BertheFRS20}. Les langages des systèmes
de numération sont souvent gouvernés par des relations
lexicographiques avec des suites, et ils sont réguliers si et
seulement si ces suites sont ultimement périodiques. Si les suites
sont purement périodiques, les représentations forment même un shift
de type fini %% ; cela se
%% passe
%% dans la $\beta$-numération décalée par un paramètre $\alpha$ pour un
%% ensemble dense de bases $\beta$ et de paramètres $\alpha$
\cite{MR3937681}. Du point de vue de la mesure par contre, cet
ensemble a une mesure nulle. Cependant, on peut décrire beaucoup de
propriétés des systèmes dynamiques si les suites coïncident
ultimement, et c'est une propriété prévalante dans le cas des
$\alpha$-fractions continues~\cite{MR4281424}, comme dans d'autres
systèmes de type \bsc{Pisot}.

Un autre sujet étroitement lié à ces question concerne la
compréhension de l'aléatoire, en particulier dans ses déclinaisons les
plus faibles. Ainsi \cite{DBLP:journals/mst/BecherCH18} identifie le
lien entre normalité d'une séquence et la non-compressibilité par
automate finis, et \cite{MR4294801} s'intéresse à la
notion de discrépance.

%Références qui peuvent être utilisées: 
\ignorecite{MR3777526,% Wieb Bosma, Michel Dekking, and Wolfgang Steiner. “A remarkable integer sequence related to π and
DBLP:conf/analco/RotondoV17,% Pablo Rotondo and Brigitte Vallée. “The recurrence function of a random Stur- mian word”. In: 2017 Proceedings of the Fourteenth Workshop on Analytic Algorithmics and Combinatorics (ANALCO). Ed. by Conrado Martínez and Mark Daniel Ward. Équipes automates, combi. SIAM, 2017, pp. 100–114.
DBLP:conf/aofa/BertheCPRSV20,% Valérie \bsc{Berthé}, Eda Cesaratto, Frédéric Paccaut, Pablo Rotondo, Martín D. Safe, and Brigitte Vallée. “Two Arithmetical Sources and Their Associated Tries”. In: 31st International Conference on Probabilistic, Combinatorial and Asymptotic Methods for the Analysis of Algorithms. Ed. by Michael Drmota and Clemens Heuberger. Vol. 159. LIPIcs. Leibniz Int. Proc. Inform. Équipes automates, combi. Schloss Dagstuhl. Leibniz-Zent. Inform., Wadern, 2020, 4:1–4:19.
MR3799923% Valérie \bsc{Berthé} and Michel Rigo. “General framework”. In: Sequences, groups, and number theory. Trends Math. Équipes automates, combi. Birkhäuser/Springer, Cham, 2018, pp. 1–36.
}


\subparagraph*{Autres modèles de calcul.}
D'autres modèles de calcul sont étudiés au sein de l'équipe, qui
correspondent à des machines de natures très diverses mais au pouvoir
d'expression limité, qui en ce sens appartiennent à l'univers de la
théorie des automates et de la vérification. C'est le cas par exemple
de modèles d'automates distribués sur les
graphes \cite{DBLP:conf/icalp/Reiter17}, des automates cellulaires sur
des pavages carrés on non \cite{DBLP:conf/ic-nc/MaignanY18}, ou les
équations différentielles (vues comme modèles de calcul
analogique) \cite{DBLP:journals/lmcs/BournezP19} ou
hybrides \cite{DBLP:conf/concur/MajumdarOP020}, ou encore les
automates à plusieurs piles \cite{DBLP:journals/ijfcs/AtigBH17}. Les
classes de circuits algébriques sont également
étudiées \cite{DBLP:journals/cjtcs/LagardeMP19}, ainsi que les schémas
récursifs d'ordre supérieurs qui permettent d'étudier la structure des
appels récursifs dans les langages fonctionnels d'ordre supérieur ;
l'approche «~opérationnelle~» de~\cite{DBLP:journals/tocl/HagueMOS17}
complémente celle plus «~dénotationnelle~» explorée dans
l'équipe \algebre~\cite{DBLP:conf/lics/Mellies17}.



\paragraph{Animation scientifique de l’équipe}

\hceresbis{Suggestion : parler animation interne (groupe de lecture/travail, séminaire). 
Pour l’animation externe, aller en Auto-évaluation 2.1 (Rayonnement) (Master/Ecole, mais aussi externe dans la communauté scientifique (écoles, réseaux, rencontres, conférences, …))
}

L'équipe se structure autour d'un séminaire hebdomadaire, dont les
sujets couvrent les différents thèmes de l'équipe.  Depuis le COVID,
le séminaire est retransmis par internet (bien que le lien ne soit pas
publiquement disponible sur le site), ce qui permet aux membres de
l'équipe de suivre malgré des empêchements, et d'inviter des
auditeur·ice·s extérieur·e·s au laboratoire.  Il arrive également que
l'orateur·ice soit à distance, et que l'équipe soit rassemblée dans
notre salle de réunion pour l'écouter (ce qui donne une expérience
bien meilleure que d'écouter depuis son bureau).  Plusieurs membres de
l'équipe participent également à d'autres séminaires, notamment ceux
de vérification, le séminaire PPS, et le <<~\emph{One World Numeration
Seminar}~>>. Depuis octobre 2022, le séminaire d'équipe est aussi
précédé par
une \href{https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata}{réunion
d'équipe} d'une demi-heure, qui est un temps d'information mais aussi
d'échanges courts sur des questions ouvertes ou des travaux en cours,
et des groupes de lecture ponctuels sont occasionnellement lancés.  Enfin,
l'équipe participe à plusieurs projets ANR qui recouvrent toutes les
sensibilités thématiques et donnent régulièrement lieu à des réunions
entre les membres de l'équipe et la communauté française.

L'équipe est aussi impliquée dans la formation à la recherche, dans
le master MPRI (Valérie \bsc{Berthé}, Olivier \bsc{Carton},
Sam \bsc{van Gool}, Paul-André \bsc{Melliès}, Sylvain \bsc{Perifel},
Daniela \bsc{Petri\c san}, Matthieu \bsc{Picantin},
Amaury \bsc{Pouly}, Olivier \bsc{Serre}), le LMFI
(Thomas \bsc{Colcombet}), le master d'informatique de l'Université
Paris Cité, mention Informatique Fondamentale
(Amélie \bsc{Gheerbrant}) et le Master FESUP de préparation à
l'agrégation d'informatique (Olivier \bsc{Carton}, Sam \bsc{van Gool},
Sylvain \bsc{Schmitz}).

Enfin, les membres du laboratoire, et de l'équipe en particulier,
participent traditionnellement au pilotage de
l'\href{https://epit.irif.fr/}{EPIT} (Valérie \bsc{Berthé},
Daniela \bsc{Petri\c san} et Cristina \bsc{Sirangelo} pour la période
couverte par le rapport), et ont monté dans ce cadre
une \href{https://conferences.cirm-math.fr/1934.html}{école sur les
bases de données et les automates} en 2019 au CIRM
(Amélie \bsc{Gheerbrant} et Cristina \bsc{Sirangelo}).  %Ils ont
%également organisé l'école <<~\emph{Mathematical Programming and
%Algorithms}~>> (CIMPA, Valérie \bsc{Berthé}) et la
Jean-Baptiste \bsc{Yunès} a également organisé la <<~\href{https://indico.ictp.it/event/9815/overview}{\emph{Mathematical
Programming and Algorithms - an African Mathematical School}}~>> en
2022.

\section{Profil d'activités liées à la recherche \trad{1/2 page}}

\hceresbis{Il s’agit d’un simple tableau à remplir, sans texte.}
\hceres{La définition du profil permet de caractériser, selon sept grandes catégories (classées par ordre alphabétique), l’ensemble des activités conduites par le collectif de recherche. Le profil d’activités est décliné à l’échelle de l’équipe, puis il sera repris à l’échelle de l’unité.
Ne pas passer beaucoup de temps à remplir ce tableau. On peut avoir des équipes de profils très différents : cela peut être utile au comité pour moduler son appréciation en fonction de ce profil.}
\bigskip
\begin{center}\footnotesize
\begin{tabular}{|p{14cm}|c|}
	\hline
	\rowcolor{black!20} 
	 \multicolumn{2}{|c|}{ {\bf Activités} (répartir 100 points sur ces 7 items)}  \\
	\hline
	\textbf{Administration et animation de la recherche~:} pilotage de la
	recherche (VP, direction d'institut, DAS, par exemple), participation à
	des instances d'évaluation (CNU, CoNRS, CSS, Hcéres, par exemple),
	responsabilité de dispositifs Idex ou Isite, direction de projets (ANR,
	Horizon Europe, ERC, CPER, PIA, France 2030, par exemple),
	responsabilités éditoriales dans des revues ou collections nationales et
	internationales. & 20 \\
	\hline
	\textbf{Aide aux politiques publiques et expertise technique~:} pouvoirs
	publics aux niveaux européen, national et régional, entreprises,
	instances internationales comme FAO, OMS, etc. & 0 \\
	\hline
	\textbf{Contribution à l'adossement d'enseignements innovants à la
	recherche~:} EUR, SFRI, etc. & 0 \\
	\hline
	\textbf{Dissémination de la recherche~: } partage de connaissances avec
	le grand public, médiation scientifique, interface sciences et société & 10 \\
	\hline
	\textbf{Recherche et encadrement de la recherche.} & 65 \\
	\hline
	\textbf{Valorisation, transfert, innovation.} & 5 \\
	\hline
	\textbf{Autres activités~:} (à préciser en une ligne maximum). & 0 \\
	\hline
\end{tabular}
\end{center}

\section{Prise en compte des recommandations du précédent rapport \trad{1/2 page}}

\hceres{L’équipe présente de façon synthétique les actions entreprises pour mettre en œuvre les recommandations de la précédente évaluation à l’échelle de l’équipe. Elle en évalue les résultats. Pour chaque recommandation : faire un rappel synthétique, décrire les actions mises en place pour répondre à cette recommandation et indiquer les résultats obtenus.}
\recom{«~L'équipe est encouragée à continuer à faire de l'excellente recherche. Elle doit mener une réflexion sur son projet scientifique collectif. Nous encourageons l’équipe à renforcer la synergie entre les divers sous-groupes qui la composent.~»}

L'équipe a cherché à maintenir en tous points la qualité de la recherche menée. 

Les sous-groupes présents dans l'équipe automate sont toujours aussi
divers, et cela fait partie de la richesse de cette équipe. Elle
représente bien l'ubiquité de la notion mathématique d'automate, que
l'on retrouve en algèbre, en logique, en combinatoire, ou dans de
nombreuses applications comme la vérification, l'algorithmique, etc.
L'équipe \automates\ offre un environnement pour l'échange entre cette
multitude de points de vue, et de nombreuses interactions existent. %%  La
%% construction d'un projet scientifique plus abouti et précis ne semble
%% pas nécessairement pertinent dans ce cadre, et risquerait de fracturer
%% cet environnement.
En pratique, l'équipe \automates\ entretient des liens très forts avec
d'autres équipes du laboratoire, de tous les pôles, et cela se traduit
par l'appartenance de nombre de ses membres à d'autres équipes, comme
l'équipe \combi, l'équipe \verif\ et l'équipe \algebre. Dans chacun de
ces sous-groupes, les membres se perçoivent comme fortement attachés à
l'équipe \automates\ comme à leur autre équipe. Depuis octobre 2022,
le séminaire d'équipe est aussi précédé par
une \href{https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata}{réunion} d'une demi-heure qui vise entre autres à renforcer la
cohésion de l'équipe.

Le cas des bases de donnée est un peu différent ; au-delà de la forte
proximité avec les automates, l'objectif est à terme que ce sujet
devienne indépendant dans ce qui serait une <<~équipe données~>>. Les
aléas des recrutements n'ont pas permis pour l'instant d'atteindre une
masse critique permettant la concrétisation de ce projet.




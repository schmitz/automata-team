---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### October 7, 2022

---

## Team Life

 - [IRIF chat](https://www.irif.fr/intranet/chat) via [Zulip](https://irif.zulipchat.com/)
---
## Sabbaticals

 - [CRCT CNU](https://cnu27.univ-lille.fr/crct.html): submit on [ANTARES](https://galaxie.enseignementsup-recherche.gouv.fr/antares/ech/index.jsp) by **Oct. 18**
 - CNRS delegation: submit on [SIRAH](https://www.galaxie.enseignementsup-recherche.gouv.fr/ensup/cand_acc_delegation_CNRS.htm) by **Oct. 18**
 
   Especially for [International Research Labs](https://www.ins2i.cnrs.fr/fr/international)

---
## Positions

 - [shared pad](https://mypads2.framapad.org/mypads/?/mypads/group/pads-sq5rpu98o/pad/view/applications-2023-035rqu99o) (password: `I<3Automata`) currently lists
   - [Sarah Winter](https://sarahwinter.net/) → seminar on Dec. 9
   - [Ismaël Jecker](https://ismaeljecker.github.io/)?
   - [Marco Abbadini](https://marcoabbadini-uni.github.io/)?
   - [Anantha Padmanabha](https://padmanabha-anantha.github.io/website/) → will apply to CNRS
   - [Carl-Fredrik Nyberg Brodda](mailto:carl-fredrik.nybergbrodda@manchester.ac.uk)? → seminar on Dec. 16

---
### Visits & invitations

 - UPC SRI invitations: 1–2 months, Mar. 2023
 - [FSMP chairs](https://sciencesmaths-paris.fr/en/nos-programmes-en/les-chaires-fsmp): 6–12 months, submit by **Nov. 30**
 - [FSMP distinguished professor fellowships](https://sciencesmaths-paris.fr/en/nos-programmes-en/fsmp-distinguished-professor-fellowship): 2–3 months, rolling deadline
 - [PGSM Master](https://sciencesmaths-paris.fr/en/nos-programmes-en/pgsm-master): grants for Master students, rolling deadline
 - [FSMP PhD stays](https://sciencesmaths-paris.fr/en/nos-programmes-en/sejours-de-doctorants): for stays outside Paris, rolling deadline
 - [visite doctorant·e du GdR-IM](https://www.gdr-im.fr/programme-visite-de-doctorante/): both directions, rolling deadline
 
---
## ?

 - who will welcome visitors?
 - anyone would like to give a talk?
 - what can you tell us about the last conference you attended?
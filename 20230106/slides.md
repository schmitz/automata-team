---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### January 6, 2023

---
# Lab life

 🎉 Happy new year!
 
 - Jan. 9, 16:15: galette de l'UFR

---
# Student contacts

 - Luc Passemard:
   - M1 student at X
   - looking for a 16-weeks internship Mar.–Aug. 2023
   - excellent grades in theoretical computer science
   - attracted to automata, logics, graph theory

 - details sent to pole-asv-permanents@irif.fr
 - anyone with a proposal?
 
---
# Student contacts

 - Olivier Idir:
   - M2 student at ENS Lyon
   - looking for a PhD next year; likely to have his own funding
   - attracted to games, automata
   - recommendations by D. Kuperberg & A. Renzaglia

 - will send details to eq-automates-permanents@irif.fr
 - anyone with a proposal?
   
---
# CNRS Applications

 - people I think have applied
   - [Marco Abbadini](https://marcoabbadini-uni.github.io/) (CR)
   - [Anantha Padmanabha](https://padmanabha-anantha.github.io/website/) (CR)
   - [Yann Ramusat](https://yannramusat.github.io/) (CR)
   - [Mirna Džamonja](https://www.logiqueconsult.eu/) (DR)
 - anyone missing?
 - who is following each application?

---
## Upcoming Conference Deadlines

 - [VLDB 2023](https://vldb.org/2023/), Vancouver, every 20th until March
 - [LICS 2023](https://lics.siglog.org/lics23/), Boston, deadline Jan. 18
 - [FSCD 2023](https://easyconferences.eu/fscd2023/), Rome, deadline Jan. 30
 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, deadline Feb. 11
 - [KR 2023](https://kr.org/KR2023/), Rhodes, deadline Mar. 7

---
## Upcoming Conferences

 - [POPL 2023](https://popl23.sigplan.org/), Boston, Jan. 15–21
 - [CSL 2023](https://csl2023.mimuw.edu.pl/), Warsaw, Feb. 13–16
 - [STACS 2023](https://www.conferences.uni-hamburg.de/event/272/), Hamburg, Mar. 7–10 
 - [ETAPS 2023](https://etaps.org/2023/), Paris, Apr. 22–27

Anyone planning on attending?

Give a short report on the conference for the team?
 
---
# Visitors

  Who will welcome visitors soon?

 - [George Metcalfe](https://www.math.unibe.ch/ueber_uns/personen/prof_dr_metcalfe_george/index_ger.html) and [Simon Santschi](https://www.math.unibe.ch/ueber_uns/personen/santschi_simon/index_ger.html) Jan. 23–28
 - [León Bohn](https://leonbohn.de/) Mar. 27–31

---
# HCERES

 - will share with eq-automates-permanents@irif.fr some data to be checked
 - additional information to be collected on [a shared document](https://docs.google.com/spreadsheets/d/1hDEUX6nWbsf6MIQ-nIgtB1IEBSwRwJzvKCcZZZ6EcWQ/edit?usp=sharing)

---
## ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
### History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
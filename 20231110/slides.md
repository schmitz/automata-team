---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### November 10, 2023

---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Questions

 - received 4 questions for the team
 - questions based on the report
 - the next 4 slides show the proposed answers—your comments are most welcome!
 
---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Questions (1/4)
<style scoped>
p {
  font-size: 16pt;
  text-align: left;
  color: #333;
}
h2 + p {
  color: #b81e0d;
}
</style>
Quel est le positionnement de l'équipe par rapport au thème des données ?

Le thème des données dans l’équipe _Automates et applications_ est apparu grâce aux arrivées d’Amélie Gheerbrant et Cristina Sirangelo en 2013 et 2015. Ce thème gagne actuellement en importance grâce au recrutement de Leonid Libkin sur une chaire industrielle et aux multiples encadrements de Master et de thèse qui débutent.

De part ses forts liens avec la théorie des modèles finis et divers modèles d’automates, il s’agit d’une des thématiques qui structurent l’équipe, au même titre que par exemple celle en lien avec la combinatoire des mots et des systèmes de numération (avec l’équipe _Combinatoire_) ou celle en lien avec les fondements algébriques, topologiques, et catégoriques de la théorie des automates (avec l’équipe _Algèbre et calcul_).  À la différence de ces deux autres thématiques, le thème des données n’est pas à l’interface avec une autre équipe de l’IRIF, et pourrait devenir à l’avenir une équipe indépendante.

À noter que les exposés du séminaire de l’équipe _Automates et applications_ portant sur la théorie des bases de données font maintenant l’objet d’un affichage commun avec d’autres séminaires actifs en Île-de-France (https://db-in-paris.gitlab.io/db-theory-in-paris/talks/).

---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Questions (2/4)
<style scoped>
p {
  font-size: 16pt;
  text-align: left;
  color: #333;
}
h2 + p {
  color: #b81e0d;
}
</style>
Concernant la thèse en cours d'Alexandra Rogova (encadrée par Amélie Gheerbrant), n'y avait-il pas de possibilité de thèse CIFRE avec neo4j, l'entreprise qui a co-encadré son stage ?

La possibilité d’une thèse CIFRE avec neo4j avait bien été envisagée. Alexandra Rogova ayant été récompensée par un financement via l’École Doctorale, et la mise en place d’une thèse CIFRE avec une entreprise non basée en France étant administrativement complexe, cette possibilité n’a finalement pas été retenue.

---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Questions (3/4)
<style scoped>
p {
  font-size: 16pt;
  text-align: left;
  color: #333;
}
h2 + p {
  color: #b81e0d;
}
</style>

Sur l'axe thématique _Autres modèles de calcul_, quel serait le résultat marquant obtenu durant la période d'évaluation ?

Les résultats sur les autres modèles de calcul mentionnés parmi les avancées scientifiques majeures dans la période d’évaluation portent sur des modèles variés. La publication [Rei17] a été en particulier récompensée par le prix du meilleur article étudiant à ICALP 2017 (track B) et porte sur des modèles de réseaux d’automates acycliques, communicant de manière asynchrone.

---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Questions (4/4)
<style scoped>
p {
  font-size: 16pt;
  text-align: left;
  color: #333;
}
h2 + p {
  color: #b81e0d;
}
</style>

La notion d'automates d'arbres est une généralisation naturelle des automates sur les mots finis ou infinis. On la retrouve au sein de l'équipe dans l'interaction avec la logique et dans celle avec l'algèbre. Qu'en est-il pour les autres axes de l'équipe ?

La notion d’automates d’arbres apparaît naturellement dans l’axe données de l’équipe, via le traitement des données XML, mais il s’agit d’une thématique en perte de vitesse dans cette communauté, qui n’est plus aussi activement étudiée ; voir par exemple [**Amélie Gheerbrant** et Leonid Libkin.  « Certain Answers over Incomplete XML Documents: Extending Tractability Boundary ».  _Theory of Computing Systems_, 57(4):892–926, 2015] pour une publication hors période d’évaluation mais en lien avec les questions de données incomplètes (qui ont été étudiées pendant la période).

Une généralisation des langages réguliers d’arbres est celle des langages des schémas récursifs d’ordre supérieur, aussi étudiés via des modèles idoines d’automates à pile d’ordre supérieur [HMOS17]. Une autre généralisation (reliée à la précédente) consiste à considérer des langages réguliers non pas d’arbres mais de λ-termes, dans la continuation de [Mel17], et les travaux de thèse de Vincent Moreau sur les λ-termes profinis s’appuient sur cette généralisation.

---
## ![height:42px](https://sciencesmaths-paris.fr/images/tmpl/menu-logo.png) FSMP

 - [Chairs](https://www.sciencesmaths-paris.fr/en/?view=article&id=327), deadline **Nov. 30**
 - [Postdocs](https://sciencesmaths-paris.fr/en/?view=article&id=326) and [Cofund Postdocs](https://www.mathingp.fr/), deadline **Nov. 30**
 - [PGSM](https://sciencesmaths-paris.fr/en/pgsm-master) funding for Masters, deadline **Feb. 2**
 
---
## ![height:42px](https://www.ins2i.cnrs.fr/sites/institut_ins2i/files/logo/CNRS_SCIENCES_INFOR_DIGI_V_RVB.svg) AAP INS2I

By the end of the month; contact me if looking to
 1. build international relations
 4. get funding for newly hired
 5. organise scientific events
 
---
## 📅 Upcoming Deadlines

 - [PODS 2024](https://2024.sigmod.org/calls_papers_pods_research.shtml), Santiago, Chile, deadline Dec. 4
 - [ESSLLI 2024](https://2024.esslli.eu/), Leuven, Belgium, deadline for course titles Dec. 15 and course proposals Jan. 12
 - [LICS 2024](https://lics.siglog.org/lics24/), Talinn, Estonia, deadline Jan. 21
 - [ICALP 2024](https://compose.ioc.ee/icalp2024/), Talinn, Estonia, deadline Feb. 14
---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
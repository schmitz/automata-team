---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### September 22, 2023
---
## 📰 Automata News

 - looking for **seminar speakers**: contact [Alexandra](https://www.irif.fr/~rogova/) and [Marie](https://www.csc.liv.ac.uk/~mfortin/)
 
 - team repository of **internship/PhD proposals**?
   - **Hichma Kari**, 4th y. @ ENS Lyon, looking for 4–6 mo. internship

 - launch of [Database Theory in Paris](https://db-in-paris.gitlab.io/db-theory-in-paris/talks/)
---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Visit

 - on [Nov. 28–30](https://www.irif.fr/intranet/hceres/2017-22/index)
   - _Tue. 28, 13:55–15:55_: scientific presentations
   - _Tue. 28, 16:25–17:35_: **ASV pole presentations**
   - _Wed. 29, 11:30–12:00_: discussions with non-permanent staff representatives
   - _Wed. 29, 12:00–13:30_: poster session
   - _Wed. 29, 16:20–16:50_: discussions with permanent staff representatives
---
## 💰 Funding

 - ![height:26px](https://anr.fr/typo3conf/ext/anr_skin/Resources/Public/assets/img/anr-logo-2021-complet.png) [ANR generic proposals](https://anr.fr/fr/detail/call/aapg-appel-a-projets-generique-2024/), deadline Oct. 19
 - ![height:36px](https://www.horizon-europe.gouv.fr/sites/default/files/2021-05/Logo_Horizon_Europe_1_2.png) [ERC starting grants](https://www.horizon-europe.gouv.fr/calendriers-et-documents-des-appels-erc-27857), deadline Oct. 24
 - ![height:36px](https://www.horizon-europe.gouv.fr/sites/default/files/2021-05/Logo_Horizon_Europe_1_2.png) [MSCA and Citizens](https://www.horizon-europe.gouv.fr/actions-marie-sklodowska-curie-et-les-citoyens-msca-and-citizens-27893), deadline Oct. 25
 
 - ![height:36px](https://www.dagstuhl.de/_Resources/Static/Packages/Dagstuhl.Site/Frontend/images/LZI-Logo.jpg?bust=b988b355) [Dagstuhl Seminar Proposals](https://www.dagstuhl.de/en/seminars/dagstuhl-seminars/composition-of-a-proposal), deadline Nov. 1
 - ![height:36px](https://candidatures.iufrance.fr/files/iuf-candidatures/design/images/logo_2022.png) [IUF chairs](https://candidatures.iufrance.fr/accueil.html), deadline Nov. 6
---
# Sabbaticals

 - CNRS delegations
 - CRCT sabbaticals

 applications on [Antares](https://galaxie.enseignementsup-recherche.gouv.fr/antares/ech/index.jsp) before **Oct. 20**
 
---
## 📅 Upcoming Deadlines

 - [STACS 2024](https://stacs2024.limos.fr/), Clermont-Ferrand, France, deadline Sep. 28
 - [ETAPS 2024](https://etaps.org/2024/cfp/), Luxembourg, deadline Oct. 12
 - [SIGMOD 2024](https://2024.sigmod.org/calls_papers_sigmod_research.shtml), Santiago, Chile, deadline Oct. 15
 - [PODS 2024](https://2024.sigmod.org/calls_papers_pods_research.shtml), Santiago, Chile, deadline Dec. 4
 
---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### February 24, 2023

---
## Upcoming Conference Deadlines

 - [Words 2023](https://dltwords2023.cs.umu.se), Umeå, deadline Feb. 26
 - [VLDB 2023](https://vldb.org/2023/), Vancouver, every 20th until March
 - [KR 2023](https://kr.org/KR2023/), Rhodes, deadline Mar. 7
 - [Concur 2023](https://www.uantwerpen.be/en/conferences/confest-2023/), Antwerp, deadline Apr. 24
 - [MFCS 2023](https://mfcs2023.labri.fr/), Bordeaux, deadline Apr. 24

---
## Upcoming Conferences

 - [STACS 2023](https://www.conferences.uni-hamburg.de/event/272/), Hamburg, Mar. 7–10
 - [ICDT 2023](http://edbticdt2023.cs.uoi.gr/), Iannina, Mar. 28–31
 - [JNIM 2023](https://jnim2023.sciencesconf.org/), Paris, Apr. 4–7
 - GT DAAL, Kremlin-Bicêtre, Apr. 21
 - [ETAPS 2023](https://etaps.org/2023/), Paris, Apr. 22–27

---
## Upcoming: JNIM 2023

### journées nationales du [GDR IM](https://www.gdr-im.fr/)

 - 📅 Apr. 4–7 at Grands Moulins
 - [program](https://jnim2023.sciencesconf.org/)
 - [registration](https://jnim2023.sciencesconf.org/registration)
 
---
## Upcoming: [GT DAAL](https://daal.labri.fr/)
 - 📅 Apr. 21 (right before ETAPS)
 - 🎓 invited speakers:
   - [Florent Koechlin](https://igm.univ-mlv.fr/~koechlin/)
   - [Liat Peterfreund](https://sites.google.com/view/liatpeterfreund/)
   - [Marie Van Den Bogaard](https://pagespro.univ-gustave-eiffel.fr/marie-van-den-bogaard)
   - [Noam Zeilberger](http://noamz.org/)
 - 📢 call for presentations:
   - deadline Mar. 9
   - by email uli@lrde.epita.fr

---
# Invited Professor Program

Invited professorships at U. Paris Cité
 - [submit a proposal](https://sondage.app.u-paris.fr/653794?lang=fr)
 - 📅 deadline **Mar. 20**

  
---
# HCERES

## General data

 - [current data](https://cloud.irif.fr/s/6D2ajWqXmarskTQ): to check
 - [lecture notes](https://docs.google.com/spreadsheets/d/1R4mG7EEDT80HsLyt54Im6UWOUi5Su7klefZC1Hq-Z6Y/edit#gid=0): to complete

---
# HCERES 

## Bibliography

 - full `automata` bibliography sent by Thomas
 - [team associated with each entry](https://lite.framacalc.org/publications-irif-equipes-9z74): to disambiguate
 - [spurious publications](https://lite.framacalc.org/publications-irif-equipes-9z74): to remove

---
# HCERES

 - LaTeX template online on [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres) and sent by email
   * recommended: activate your [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres) account
   * todo **now**: sec. 1.1 (`info-eqth.tex`)
     - either commit changes to [gitlab](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/tree/master/hceres)
     - or send to Thomas and me by email
   * choice of elements for the portfolio

---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
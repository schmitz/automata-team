---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### November 24, 2023

---
## ![height:60px](https://www.hceres.fr/sites/default/files/Logos/logo-hceres-new-en-couleur-transparent.png) HCERES Visit

 - on [Nov. 28–30](https://www.irif.fr/intranet/hceres/2017-22/index)
   - _Tue. 28, 10:45–10:55_: general presentations
   - _Tue. 28, 13:55–15:55_: scientific presentations
   - _Tue. 28, 16:25–17:35_: **ASV pole presentations**
   - _Wed. 29, 11:30–12:00_: discussions with non-permanent staff representatives
   - _Wed. 29, 12:00–13:30_: poster session
   - _Wed. 29, 16:20–16:50_: discussions with permanent staff representatives
---
## ![height:42px](https://sciencesmaths-paris.fr/images/tmpl/menu-logo.png) FSMP

 - [Chairs](https://www.sciencesmaths-paris.fr/en/?view=article&id=327), deadline **Nov. 30**
 - [Postdocs](https://sciencesmaths-paris.fr/en/?view=article&id=326) and [Cofund Postdocs](https://www.mathingp.fr/), deadline **Nov. 30**
 - [PGSM](https://sciencesmaths-paris.fr/en/pgsm-master) funding for Masters, deadline **Feb. 2**
 
---
## ![height:42px](https://www.ins2i.cnrs.fr/sites/institut_ins2i/files/logo/CNRS_SCIENCES_INFOR_DIGI_V_RVB.svg) AAP INS2I

By the end of the month; contact me if looking to
 1. build international relations
 4. get funding for newly hired
 5. organise scientific events
 
---
## 📅 Upcoming Deadlines

 - [PODS 2024](https://2024.sigmod.org/calls_papers_pods_research.shtml), Santiago, Chile, deadline Dec. 4
 - [ESSLLI 2024](https://2024.esslli.eu/), Leuven, Belgium, deadline for course titles Dec. 15 and course proposals Jan. 12
 - [LICS 2024](https://lics.siglog.org/lics24/), Talinn, Estonia, deadline Jan. 21
 - [ICALP 2024](https://compose.ioc.ee/icalp2024/), Talinn, Estonia, deadline Feb. 14
---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
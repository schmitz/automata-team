---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### September 30, 2022

---

## Newcomers

 - ![width:100px](https://www.csc.liv.ac.uk/~mfortin/assets/img/photo.jpg) Marie Fortin (CR CNRS)
 * who else?

---

## Team Life

 - automata seminar
 - pole day
 - team day
 - other upcoming events
 - [IRIF chat](https://www.irif.fr/intranet/chat) via [Zulip](https://irif.zulipchat.com/)
 
---

### Automata Seminar

 - organised by *Arthur Jaquard* & *Aliaume Lopez*
 - access to [videos](https://cloud.irif.fr/s/2a7zZifcRsNKsjb); password: `I<3Automata`
 - start at **14:00** instead of 14:30
 - suggestions for **speakers**?
 * add a **weekly team meeting** from **13:30** to 14:00

---
### ASV Day

 - save the date: **October 21, 2022**
 - volunteer to give a short presentation!

---
### Team Day

 - what about doing that in late May/early June?

---
### Other Upcoming Events

 - Nov. 17: workshop on Duality and Logic in the passage from the Finite to the Infinite
 - Dec. 15: Arnaud Sangnier's HDR defense
 - Jan. 16–20: [Discrete mathematics and logic](https://conferences.cirm-math.fr/2758.html) at CIRM

---
## Positions at IRIF

 * 3 MCF + 2 PR seems likely
 * also attract CNRS applicants
 * let's be proactive: who can we attract?
   [shared pad](https://mypads2.framapad.org/mypads/?/mypads/group/pads-sq5rpu98o/pad/view/applications-2023-035rqu99o); password: `I<3Automata` 
 * [qualification CNU 27](https://cnu27.univ-lille.fr/qualification.html):
   - register by **Nov. 8**
   - submit by **Dec. 15** (Jan. 12 if PhD defended after Nov. 22)

---
## Funding

 * ERC
   - [starting grants](https://erc.europa.eu/apply-grant/starting-grant): by **Oct. 25**
   - [synergy grants](https://erc.europa.eu/apply-grant/synergy-grant): by **Nov. 8**
   - [consolidator grants](https://erc.europa.eu/apply-grant/consolidator-grants): by **Feb. 2**
 * [ANR](https://anr.fr/fr/detail/call/aapg-appel-a-projets-generique-2023/): by **Nov. 7**
 * UPC AAP Émergence: soon

---
### Funding for Postdocs

 * [IRIF webpage](https://www.irif.fr/postes/postdoc)
 * [Paris Region Fellowships](https://parisregion.eu/parisregionfp.html): **Oct. 21**
 * [FSMP Postdoc](https://sciencesmaths-paris.fr/en/nos-programmes-en/postdocs) & [Cofund](https://www.mathingp.fr/): by **Nov. 30**
 * [Marie Curie Fellowships](https://marie-sklodowska-curie-actions.ec.europa.eu/actions/postdoctoral-fellowships): next call in April for next September

---
## HCERES

 - IRIF is in the next evaluation
 - more information in October
 - please check your work is referenced on the [HAL IRIF](https://hal.archives-ouvertes.fr/IRIF)
 
---
## Communication

 - [Twitter](https://twitter.com/IRIF_Paris)
 - **PEPIT**
   - **P**ublications: scientific results
   - **E**vents: organisation of conferences, workshops, …
   - **P**rizes: best paper, distinctions, fellow
   - **I**nnovation: patents, industrial collaborations, …
   - **T**echnology: software
 - send an email to [communication@irif.fr](mailto:communication@irif.fr)

---
## ? <!--fit-->
---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### May 26, 2023
---
## 📅 ASV meeting

 - Tuesday, June 20, 2pm–4pm

---
## 📅 Upcoming Deadlines

 - [PODS 2024](https://2024.sigmod.org/calls_papers_pods_research.shtml), Santiago, Chile, deadline June 6
 - [FSTTCS 2023](https://www.fsttcs.org.in/2023/), Hyderabad, India, deadline July 12
 - [CSL 2024](https://csl2024.github.io/Home/), Naples, Italy, deadline July 24

---
## 📅 Upcoming Conferences

 - [PODS 2023](https://2023.sigmod.org/), Seattle, USA, Jun. 18–23
 - [EJCIM 2023](https://ejcim23.sciencesconf.org/), Poitiers, Jun. 19–23
 - [LICS 2023](https://lics.siglog.org/lics23/), Boston, USA, Jun. 26–29
 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, Germany, Jul. 10–14
 - [HCRW 2023](https://highlights-conference.org/2023/hcrw), Kassel, Germany, Jul. 17–23
 - [CAV 2023](http://www.i-cav.org/2023/), Paris, France, Jul. 17–22
 - [Highlights 2023](https://highlights-conference.org/2023/), Kassel, Germany, Jul. 24–28
 - [MFCS 2023](https://mfcs2023.labri.fr/), Bordeaux, France, Aug. 28–Sep. 1
 - [VLDB 2023](https://vldb.org/2023/), Vancouver, Canada, Aug. 28–Sep. 1
 - [KR 2023](https://kr.org/KR2023/), Rhodes, Greece, Sep. 2–8
 
---
# Teaching @ CS dpt

 - Uni employees: ongoing bidding on [silice](https://silice.informatique.univ-paris-diderot.fr/ufr/)
 - CNRS employees: contact the [CS department](mailto:direction-ufr@informatique.univ-paris-diderot.fr) if you'd like to teach
 - ⚠️ please keep Friday afternoons **free** to attend the meeting and seminar

---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
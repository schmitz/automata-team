---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### November 18, 2022

---
## Team Life

 - **AG IRIF** next Tuesday Nov. 22
 - team newcomer: welcome to [Florian Renkin](https://www.lrde.epita.fr/~frenkin/)
 - IRIF web pages: it's recommended to have a professional web page; see [instructions](https://www.irif.fr/intranet/wiki)
---
# M2 Internships

 Time to offer M2 internships!
 
 Contact points with courses at [MPRI](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=start) and [LMFI](https://master.math.univ-paris-diderot.fr/annee/m2-lmfi/):
 - [Thomas Colcombet](https://master.math.univ-paris-diderot.fr/modules/m2lmfi-second-ordre/)
 - [Daniela Petrisan, Matthieu Picantin, Amaury Pouly, and Sam van Gool](https://wikimpri.dptinfo.ens-cachan.fr/doku.php?id=cours:c-2-16)
---
# Organising Workshops

 - [LICS workshops](https://lics.siglog.org/lics23/cfw.php) deadline Nov. 30
 - [ICALP workshops](https://icalp2023.cs.upb.de/workshops/) deadline Dec. 9
 - [IES Cargèse](https://iesc.universita.corsica/?lang=en) deadline Dec. 5
 - [CIRM](https://www.cirm-math.com/)
 - [Dagstuhl](https://dosa.dagstuhl.de/)
 - [Les Houches](https://www.houches-school-physics.com/)???

---
# Visitors

  Who will welcome visitors soon?

 - [Moses Ganardi](https://people.mpi-sws.org/~mganardi/) Nov. 25
 - [Sarah Winter](https://sarahwinter.net/) Dec. 6–9
 - [Carl-Fredrik Nyberg Brodda](https://sites.google.com/view/cf-nb/?pli=1) Dec. 16

---
## ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?
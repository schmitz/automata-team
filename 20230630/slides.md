---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### June 30, 2023
---
# IRIF End-of-Year Meeting

 - July 7 in the Olympe de Gouge building
   
---
## 📅 Upcoming Deadlines

 - [FSTTCS 2023](https://www.fsttcs.org.in/2023/), Hyderabad, India, deadline July 12
 - [CSL 2024](https://csl2024.github.io/Home/), Naples, Italy, deadline July 24
---
## 📅 Upcoming Events

 - [ICALP 2023](https://icalp2023.cs.upb.de/), Paderborn, Germany, Jul. 10–14
 - [HCRW 2023](https://highlights-conference.org/2023/hcrw), Kassel, Germany, Jul. 17–23
 - [CAV 2023](http://www.i-cav.org/2023/), Paris, France, Jul. 17–22
 - [Highlights 2023](https://highlights-conference.org/2023/), Kassel, Germany, Jul. 24–28
 - [ESSLLI 2023](https://2023.esslli.eu/), Ljubljana, Jul. 31–Aug. 11
 - [MFCS 2023](https://mfcs2023.labri.fr/), Bordeaux, France, Aug. 28–Sep. 1
 - [VLDB 2023](https://vldb.org/2023/), Vancouver, Canada, Aug. 28–Sep. 1
 - [KR 2023](https://kr.org/KR2023/), Rhodes, Greece, Sep. 2–8
---
## In-Person Conference Attendance

 - IRIF [supports](https://www.irif.fr/intranet/irif-environnement/texte-conferences-v2) its members wishing to attend conferences remotely
---
# Summer Closures

 - Université Paris Cité closes from Jul. 26 to Aug. 15
 - [access request](https://sdstm.math-info-paris.cnrs.fr/shared/NHNluwif6TyYlgZD8WjBKQP7LRlJa5MGTP5dFLxbq5E) before **Jul. 8**
---
# ?

 - anyone would like to give a talk?
 - any paper(s) in preparation?
 - start a reading group on a topic?

---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)
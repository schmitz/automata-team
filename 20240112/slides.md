---
marp: true
theme: uncover
size: 16:9
paginate: true
---
<style>
section h1, section h2, section h3, section h4, section a, section a:hover
  { color: #b81e0d; }
</style>

<!-- _backgroundImage: url('https://marp.app/assets/hero-background.svg') -->
![bg fit left:32%](https://www.irif.fr/~schmitz/IRIF_full.svg)
# Automata & Applications

###### January 12, 2024

---
## 📢 Open Positions in 2024

 - 3 tenured positions in the CS department/IRIF
 - open to all the fields in theoretical computer science
   - 1 full professor position
   - 2 associate professor positions
 - **advertise** to your colleagues!
 
---
## ![height:48px](https://www.cnrs.fr/sites/default/files/logo/logo.svg) CNRS Applications

 - deadline for applications **Feb. 9**
 - talks by applicants in the team:
   - Dec. 8, 2pm: [Anantha Padmanabha](https://padmanabha-anantha.github.io/website/#/)
   - Dec. 15, 2pm: [Chana Weil-Kennedy](https://chana-wk.github.io/)
   - Dec. 18, 11am: [Munyque Mittelmann](https://sites.google.com/view/mittelmann)
   - Jan. 15, 11am: [Raphaël Berthon](https://moves.rwth-aachen.de/people/raphael-berthon/)
 - also [Emily Clement](https://perso.eleves.ens-rennes.fr/people/emily.clement/), [Tito Nguyễn](https://nguyentito.eu), and [Herman Goulet-Ouellet](https://hermangouletouellet.github.io)

---
## Émergence Funding

 - [IdEx Émergence](https://u-paris.fr/appel-emergence-en-recherche-2024/) by **Feb. 23**, 11:00
   - ⚠ submit both `.pdf` and `.doc/.odt/.docx`
   - 📅 1–2 years
   - 💰 k€10–40 total funding
   - 🧑‍🏫 teaching waiver (to be negociated with the UFR)
 - simultaneous submission to [IRIF grants](https://www.irif.fr/intranet/irif_grant)
 
Who may apply?

---
## Sabbaticals

 - congés pour projet pédagogique (CPP)
 - congés pour recherches et conversions thématiques (CRCT)
 - deadline **Feb. 23 2024, 4PM** on [NAOS](https://galaxie.enseignementsup-recherche.gouv.fr/antares/ech/index.jsp)

---
## MPRI Potential Internships

 - [Jean Abou Samra](https://jean.abou-samra.fr/cv-schmitz/): wqos and applications
 - Émile Larroque: fast-growing complexity, potential connections to quantum complexity

---
## Automata Seminar

 - [Marie Fortin](https://www.irif.fr/~mfortin/) and [Alexandra Rogova](https://www.irif.fr/~rogova/) would welcome your suggestions for speakers starting in February next year

---
## 📅 Upcoming Deadlines

 - [LICS 2024](https://lics.siglog.org/lics24/), Talinn, Estonia, deadline Jan. 21
 - [ICALP 2024](https://compose.ioc.ee/icalp2024/), Talinn, Estonia, deadline Feb. 14
 
---
# History

 find [past slides](https://gaufre.informatique.univ-paris-diderot.fr/schmitz/automata/)